package com.manager.estate.provider;

import com.manager.estate.feature.apartment.dao.ApartmentRepository;
import com.manager.estate.feature.apartment.model.Apartment;
import com.manager.estate.feature.invoice.dao.InvoiceRepository;
import com.manager.estate.feature.invoice.model.Invoice;
import com.manager.estate.feature.meter.dao.MeterRepository;
import com.manager.estate.feature.meter.model.Meter;
import com.manager.estate.feature.payment.dao.PaymentRepository;
import com.manager.estate.feature.payment.model.Payment;
import com.manager.estate.feature.property.dao.PropertyRepository;
import com.manager.estate.feature.property.model.Property;
import com.manager.estate.feature.readings.dao.ReadingValueRepository;
import com.manager.estate.feature.readings.dao.ReadingsRepository;
import com.manager.estate.feature.readings.model.ReadingValue;
import com.manager.estate.feature.readings.model.Readings;
import com.manager.estate.feature.tenant.dao.TenantRepository;
import com.manager.estate.feature.tenant.model.Tenant;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Transactional
@RequiredArgsConstructor
public class DataProvider {

    @Autowired
    private final PropertyRepository propertyRepository;
    @Autowired
    private final InvoiceRepository invoiceRepository;
    @Autowired
    private final MeterRepository meterRepository;
    @Autowired
    private final PaymentRepository paymentRepository;
    @Autowired
    private final ApartmentRepository apartmentRepository;
    @Autowired
    private final TenantRepository tenantRepository;
    @Autowired
    private final ReadingsRepository readingsRepository;
    @Autowired
    private final ReadingValueRepository readingValueRepository;

    @PostConstruct
    public void initData() {
        final List<Property> residences = saveProperty();
        final List<Apartment> apartments = saveApartments();
        final List<Tenant> tenants = saveTenants();
        final List<Meter> meters = saveMeters();
        final List<Readings> readings = saveReadings();
        final List<ReadingValue> readingValues = saveReadingValues();
        final List<Invoice> invoices = saveInvoices();
        final List<Payment> payments = savePayments();
    }

    private List<Apartment> saveApartments() {
        final List<Apartment> apartments = MockProvider.APARTMENTS;
        return apartmentRepository.save(apartments);
    }

    private List<Invoice> saveInvoices() {
        final List<Invoice> invoices = MockProvider.INVOICES;
        return invoiceRepository.save(invoices);
    }

    private List<Meter> saveMeters() {
        final List<Meter> meters = MockProvider.METERS;
        return meterRepository.save(meters);
    }

    private List<Payment> savePayments() {
        final List<Payment> payments = MockProvider.PAYMENTS;
        return paymentRepository.save(payments);
    }

    private List<Property> saveProperty() {
        final List<Property> property = MockProvider.PROPERTIES;
        return propertyRepository.save(property);
    }

    private List<Readings> saveReadings() {
        final List<Readings> readings = MockProvider.READINGS;
        return readingsRepository.save(readings);
    }

    private List<ReadingValue> saveReadingValues() {
        final List<ReadingValue> readingValues = MockProvider.READING_VALUES_JANUARY;
        return readingValueRepository.save(readingValues);
    }

    private List<Tenant> saveTenants() {
        final List<Tenant> tenants = MockProvider.TENANTS;
        return tenantRepository.save(tenants);
    }
}
