package com.manager.estate.feature.readings.dao;

import com.manager.estate.feature.readings.model.ReadingValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReadingValueRepository extends JpaRepository<ReadingValue, Long> {
}
