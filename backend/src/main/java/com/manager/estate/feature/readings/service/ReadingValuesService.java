package com.manager.estate.feature.readings.service;

import com.manager.estate.feature.readings.dao.ReadingValueRepository;
import com.manager.estate.feature.readings.model.ReadingValue;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ReadingValuesService {
    private final ReadingValueRepository readingValueRepository;

    public List<ReadingValue> getList() {
        return readingValueRepository.findAll();
    }

    public ReadingValue save(final ReadingValue readingValues) {
        return readingValueRepository.save(readingValues);
    }

    public void delete(final Long id) {
        readingValueRepository.delete(id);
    }
}
